import { expect, expectAsync, tap } from '@pushrocks/tapbundle';
import { Qenv } from '@pushrocks/qenv';

const testQenv = new Qenv('./', './.nogit');

import * as bobcat from '../ts/index';

let testBobcatManager: bobcat.BobcatManager;
let testBobcat: bobcat.Bobcat;

tap.test('should create a bobcat manager', async () => {
  testBobcatManager = new bobcat.BobcatManager();
  expect(testBobcatManager).toBeInstanceOf(bobcat.BobcatManager);
});

tap.test('should create a bobcat', async () => {
  testBobcat = new bobcat.Bobcat('bobcat.bleu.de');
  expect(testBobcat).toBeInstanceOf(bobcat.Bobcat);
})

tap.test('should add a Bobcat miner', async () => {
  const bobcatAddresses = testQenv.getEnvVarOnDemand('BOBCATS').split(',');
  console.log(bobcatAddresses);

  for (const bobcatAddress of bobcatAddresses) {
    await testBobcatManager.addBobcat(bobcatAddress);
  }
});

tap.test('should run maintenance on bobcats', async () => {
  await testBobcatManager.runMaintenance();
})

tap.start();
